"use strict";
var Helpers_1 = require('../helpers/Helpers');
var FilterClause = (function () {
    function FilterClause(property) {
        if (property === void 0) { property = null; }
        this.Property = property;
        this.Components = [];
    }
    ;
    FilterClause.prototype.toString = function () {
        var strComps, i, filterStr;
        strComps = [];
        if (!this.PropertyIncluded) {
            strComps.push(this.Property);
        }
        for (i = 0; i < this.Components.length; i++) {
            strComps.push(this.Components[i]);
        }
        filterStr = strComps.join(' ');
        if (!this.UsingNot) {
            return filterStr;
        }
        return typeof this.FuncReturnType === 'boolean' ? 'not ' + filterStr : 'not (' + filterStr + ')';
    };
    FilterClause.prototype.isEmpty = function () {
        return this.IsClauseEmpty || (this.PropertyIncluded && this.UsingNot);
    };
    // Logical operators
    FilterClause.prototype.eq = function (value) {
        return Helpers_1.Helpers.addLogicalOperator(value, 'eq', this);
    };
    FilterClause.prototype.ne = function (value) {
        return Helpers_1.Helpers.addLogicalOperator(value, 'ne', this);
    };
    FilterClause.prototype.gt = function (value) {
        return Helpers_1.Helpers.addLogicalOperator(value, 'gt', this);
    };
    FilterClause.prototype.ge = function (value) {
        return Helpers_1.Helpers.addLogicalOperator(value, 'ge', this);
    };
    FilterClause.prototype.lt = function (value) {
        return Helpers_1.Helpers.addLogicalOperator(value, 'lt', this);
    };
    FilterClause.prototype.le = function (value) {
        return Helpers_1.Helpers.addLogicalOperator(value, 'le', this);
    };
    FilterClause.prototype.not = function () {
        this.UsingNot = true;
        return this;
    };
    // Arithmetic methods
    FilterClause.prototype.add = function (amount) {
        return Helpers_1.Helpers.addArithmeticOperator(amount, 'add', this);
    };
    FilterClause.prototype.sub = function (amount) {
        return Helpers_1.Helpers.addArithmeticOperator(amount, 'sub', this);
    };
    FilterClause.prototype.mul = function (amount) {
        return Helpers_1.Helpers.addArithmeticOperator(amount, 'mul', this);
    };
    FilterClause.prototype.div = function (amount) {
        return Helpers_1.Helpers.addArithmeticOperator(amount, 'div', this);
    };
    FilterClause.prototype.mod = function (amount) {
        return Helpers_1.Helpers.addArithmeticOperator(amount, 'mod', this);
    };
    // String functions
    FilterClause.prototype.substringof = function (value) {
        this.PropertyIncluded = true;
        this.FuncReturnType = Boolean();
        var that = this;
        var property = this.Property;
        if (this.transformFunc !== null) {
            property = this.Components[this.Components.length - 1];
            this.Components.splice(this.Components.length - 1, 1);
        }
        this.Components.push('substringof(\'' + value + '\',' + property + ')');
        return this;
    };
    FilterClause.prototype.substring = function (position, length) {
        this.PropertyIncluded = true;
        this.FuncReturnType = String();
        var comps = [this.Property, position];
        if (length !== undefined) {
            comps.push(length);
        }
        this.Components.push('substring(' + comps.join(',') + ')');
        return this;
    };
    FilterClause.prototype.toLower = function () {
        this.PropertyIncluded = true;
        this.FuncReturnType = String();
        var that = this;
        this.transformFunc = this.toLower;
        this.Components.push('tolower(' + that.Property + ')');
        return this;
    };
    FilterClause.prototype.toUpper = function () {
        this.PropertyIncluded = true;
        this.FuncReturnType = String();
        var that = this;
        this.transformFunc = this.toUpper;
        this.Components.push('toupper(' + that.Property + ')');
        return this;
    };
    FilterClause.prototype.trim = function () {
        this.PropertyIncluded = true;
        this.FuncReturnType = String();
        var that = this;
        this.transformFunc = this.trim;
        this.Components.push('trim(' + that.Property + ')');
        return this;
    };
    FilterClause.prototype.endswith = function (value) {
        this.PropertyIncluded = true;
        this.FuncReturnType = Boolean();
        var that = this;
        this.Components.push('endswith(' + that.Property + ',\'' + value + '\')');
        return this;
    };
    FilterClause.prototype.startswith = function (value) {
        this.PropertyIncluded = true;
        this.FuncReturnType = Boolean();
        var that = this;
        this.Components.push('startswith(' + that.Property + ',\'' + value + '\')');
        return this;
    };
    FilterClause.prototype.length = function () {
        this.PropertyIncluded = true;
        this.FuncReturnType = Number();
        var that = this;
        this.Components.push('length(' + that.Property + ')');
        return this;
    };
    FilterClause.prototype.indexof = function (value) {
        this.PropertyIncluded = true;
        this.FuncReturnType = Number();
        var that = this;
        this.Components.push('indexof(' + that.Property + ',\'' + value + '\')');
        return this;
    };
    FilterClause.prototype.replace = function (find, replace) {
        this.PropertyIncluded = true;
        this.FuncReturnType = String();
        var that = this;
        this.Components.push('replace(' + that.Property + ',\'' + find + '\',\'' + replace + '\')');
        return this;
    };
    // Concat
    FilterClause.prototype.Concat = function (concat) {
        this.PropertyIncluded = true;
        this.FuncReturnType = String();
        var that = this;
        that.Components.push(concat.toString());
        return this;
    };
    // Date functions
    FilterClause.prototype.day = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'day');
    };
    FilterClause.prototype.hour = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'hour');
    };
    FilterClause.prototype.minute = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'minute');
    };
    FilterClause.prototype.month = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'month');
    };
    FilterClause.prototype.second = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'second');
    };
    FilterClause.prototype.year = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'year');
    };
    // Math functions
    FilterClause.prototype.round = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'round');
    };
    FilterClause.prototype.floor = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'floor');
    };
    FilterClause.prototype.ceiling = function () {
        return Helpers_1.Helpers.addMethodWrapper(this, 'ceiling');
    };
    return FilterClause;
}());
exports.FilterClause = FilterClause;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyY2xhdXNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZmlsdGVyY2xhdXNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSx3QkFBc0Isb0JBQW9CLENBQUMsQ0FBQTtBQUUzQztJQVdFLHNCQUFZLFFBQXVCO1FBQXZCLHdCQUF1QixHQUF2QixlQUF1QjtRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztJQUN2QixDQUFDOztJQUVELCtCQUFRLEdBQVI7UUFDRSxJQUFJLFFBQWEsRUFBRSxDQUFNLEVBQUUsU0FBYyxDQUFDO1FBQzFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFFZCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDM0IsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUVELEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDNUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUNELFNBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRS9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDbkIsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUNuQixDQUFDO1FBRUQsTUFBTSxDQUFDLE9BQU8sSUFBSSxDQUFDLGNBQWMsS0FBSyxTQUFTLEdBQUcsTUFBTSxHQUFHLFNBQVMsR0FBRyxPQUFPLEdBQUcsU0FBUyxHQUFHLEdBQUcsQ0FBQztJQUNuRyxDQUFDO0lBQ0QsOEJBQU8sR0FBUDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBSUQsb0JBQW9CO0lBQ3BCLHlCQUFFLEdBQUYsVUFBRyxLQUE0QjtRQUM3QixNQUFNLENBQUMsaUJBQU8sQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFDRCx5QkFBRSxHQUFGLFVBQUcsS0FBNEI7UUFDN0IsTUFBTSxDQUFDLGlCQUFPLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBQ0QseUJBQUUsR0FBRixVQUFHLEtBQTRCO1FBQzdCLE1BQU0sQ0FBQyxpQkFBTyxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUNELHlCQUFFLEdBQUYsVUFBRyxLQUE0QjtRQUM3QixNQUFNLENBQUMsaUJBQU8sQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFDRCx5QkFBRSxHQUFGLFVBQUcsS0FBNEI7UUFDN0IsTUFBTSxDQUFDLGlCQUFPLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBQ0QseUJBQUUsR0FBRixVQUFHLEtBQTRCO1FBQzdCLE1BQU0sQ0FBQyxpQkFBTyxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUNELDBCQUFHLEdBQUg7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELHFCQUFxQjtJQUNyQiwwQkFBRyxHQUFILFVBQUksTUFBYztRQUNoQixNQUFNLENBQUMsaUJBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCwwQkFBRyxHQUFILFVBQUksTUFBYztRQUNoQixNQUFNLENBQUMsaUJBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCwwQkFBRyxHQUFILFVBQUksTUFBYztRQUNoQixNQUFNLENBQUMsaUJBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCwwQkFBRyxHQUFILFVBQUksTUFBYztRQUNoQixNQUFNLENBQUMsaUJBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCwwQkFBRyxHQUFILFVBQUksTUFBYztRQUNoQixNQUFNLENBQUMsaUJBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxtQkFBbUI7SUFDbkIsa0NBQVcsR0FBWCxVQUFZLEtBQWE7UUFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLE9BQU8sRUFBRSxDQUFDO1FBQ2hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUVoQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzdCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNoQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQztRQUVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssR0FBRyxLQUFLLEdBQUcsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRXhFLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsZ0NBQVMsR0FBVCxVQUFVLFFBQWdCLEVBQUUsTUFBZTtRQUN6QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxFQUFFLENBQUM7UUFFL0IsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckIsQ0FBQztRQUVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRTNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsOEJBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFFaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRXZELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsOEJBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFFaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRXZELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsMkJBQUksR0FBSjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFFaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRXBELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsK0JBQVEsR0FBUixVQUFTLEtBQWE7UUFDcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLE9BQU8sRUFBRSxDQUFDO1FBQ2hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBRTFFLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsaUNBQVUsR0FBVixVQUFXLEtBQWE7UUFDdEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLE9BQU8sRUFBRSxDQUFDO1FBQ2hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBRTVFLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsNkJBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFFdEQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFDRCw4QkFBTyxHQUFQLFVBQVEsS0FBYTtRQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxFQUFFLENBQUM7UUFDL0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssR0FBRyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFFekUsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFDRCw4QkFBTyxHQUFQLFVBQVEsSUFBWSxFQUFFLE9BQWU7UUFDbkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sRUFBRSxDQUFDO1FBQy9CLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLEdBQUcsSUFBSSxHQUFHLE9BQU8sR0FBRyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFFNUYsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxTQUFTO0lBQ1QsNkJBQU0sR0FBTixVQUFPLE1BQWM7UUFDbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sRUFBRSxDQUFDO1FBQy9CLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUV4QyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGlCQUFpQjtJQUNqQiwwQkFBRyxHQUFIO1FBQ0UsTUFBTSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCwyQkFBSSxHQUFKO1FBQ0UsTUFBTSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFDRCw2QkFBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFDRCw0QkFBSyxHQUFMO1FBQ0UsTUFBTSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFDRCw2QkFBTSxHQUFOO1FBQ0UsTUFBTSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFDRCwyQkFBSSxHQUFKO1FBQ0UsTUFBTSxDQUFDLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxpQkFBaUI7SUFDakIsNEJBQUssR0FBTDtRQUNFLE1BQU0sQ0FBQyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBQ0QsNEJBQUssR0FBTDtRQUNFLE1BQU0sQ0FBQyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBQ0QsOEJBQU8sR0FBUDtRQUNFLE1BQU0sQ0FBQyxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLEFBOU5ELElBOE5DO0FBOU5ZLG9CQUFZLGVBOE54QixDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtIZWxwZXJzfSBmcm9tICcuLi9oZWxwZXJzL0hlbHBlcnMnO1xyXG5pbXBvcnQge0NvbmNhdH0gZnJvbSAnLi4vSGVscGVycy9Db25jYXQnO1xyXG5leHBvcnQgY2xhc3MgRmlsdGVyQ2xhdXNlIHtcclxuXHJcbiAgUHJvcGVydHk6IHN0cmluZztcclxuICBDb21wb25lbnRzOiBzdHJpbmdbXTtcclxuICBJc0NsYXVzZUVtcHR5OiBCb29sZWFuO1xyXG4gIFByb3BlcnR5SW5jbHVkZWQ6IEJvb2xlYW47XHJcbiAgcHJpdmF0ZSBVc2luZ05vdDogQm9vbGVhbjtcclxuICBWYWx1ZTogYW55O1xyXG4gIEZ1bmNSZXR1cm5UeXBlOiBhbnk7XHJcbiAgcHJpdmF0ZSB0cmFuc2Zvcm1GdW5jOiBGdW5jdGlvbjtcclxuXHJcbiAgY29uc3RydWN0b3IocHJvcGVydHk6IHN0cmluZyA9IG51bGwpIHtcclxuICAgIHRoaXMuUHJvcGVydHkgPSBwcm9wZXJ0eTtcclxuICAgIHRoaXMuQ29tcG9uZW50cyA9IFtdO1xyXG4gIH07XHJcblxyXG4gIHRvU3RyaW5nKCk6IHN0cmluZyB7XHJcbiAgICBsZXQgc3RyQ29tcHM6IGFueSwgaTogYW55LCBmaWx0ZXJTdHI6IGFueTtcclxuICAgIHN0ckNvbXBzID0gW107XHJcblxyXG4gICAgaWYgKCF0aGlzLlByb3BlcnR5SW5jbHVkZWQpIHtcclxuICAgICAgc3RyQ29tcHMucHVzaCh0aGlzLlByb3BlcnR5KTtcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5Db21wb25lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHN0ckNvbXBzLnB1c2godGhpcy5Db21wb25lbnRzW2ldKTtcclxuICAgIH1cclxuICAgIGZpbHRlclN0ciA9IHN0ckNvbXBzLmpvaW4oJyAnKTtcclxuXHJcbiAgICBpZiAoIXRoaXMuVXNpbmdOb3QpIHtcclxuICAgICAgcmV0dXJuIGZpbHRlclN0cjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdHlwZW9mIHRoaXMuRnVuY1JldHVyblR5cGUgPT09ICdib29sZWFuJyA/ICdub3QgJyArIGZpbHRlclN0ciA6ICdub3QgKCcgKyBmaWx0ZXJTdHIgKyAnKSc7XHJcbiAgfVxyXG4gIGlzRW1wdHkoKTogQm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5Jc0NsYXVzZUVtcHR5IHx8ICh0aGlzLlByb3BlcnR5SW5jbHVkZWQgJiYgdGhpcy5Vc2luZ05vdCk7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIC8vIExvZ2ljYWwgb3BlcmF0b3JzXHJcbiAgZXEodmFsdWU6IHN0cmluZ3xudW1iZXJ8Ym9vbGVhbik6IEZpbHRlckNsYXVzZSB7XHJcbiAgICByZXR1cm4gSGVscGVycy5hZGRMb2dpY2FsT3BlcmF0b3IodmFsdWUsICdlcScsIHRoaXMpO1xyXG4gIH1cclxuICBuZSh2YWx1ZTogc3RyaW5nfG51bWJlcnxib29sZWFuKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZExvZ2ljYWxPcGVyYXRvcih2YWx1ZSwgJ25lJywgdGhpcyk7XHJcbiAgfVxyXG4gIGd0KHZhbHVlOiBzdHJpbmd8bnVtYmVyfGJvb2xlYW4pOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgcmV0dXJuIEhlbHBlcnMuYWRkTG9naWNhbE9wZXJhdG9yKHZhbHVlLCAnZ3QnLCB0aGlzKTtcclxuICB9XHJcbiAgZ2UodmFsdWU6IHN0cmluZ3xudW1iZXJ8Ym9vbGVhbik6IEZpbHRlckNsYXVzZSB7XHJcbiAgICByZXR1cm4gSGVscGVycy5hZGRMb2dpY2FsT3BlcmF0b3IodmFsdWUsICdnZScsIHRoaXMpO1xyXG4gIH1cclxuICBsdCh2YWx1ZTogc3RyaW5nfG51bWJlcnxib29sZWFuKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZExvZ2ljYWxPcGVyYXRvcih2YWx1ZSwgJ2x0JywgdGhpcyk7XHJcbiAgfVxyXG4gIGxlKHZhbHVlOiBzdHJpbmd8bnVtYmVyfGJvb2xlYW4pOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgcmV0dXJuIEhlbHBlcnMuYWRkTG9naWNhbE9wZXJhdG9yKHZhbHVlLCAnbGUnLCB0aGlzKTtcclxuICB9XHJcbiAgbm90KCk6IEZpbHRlckNsYXVzZSB7XHJcbiAgICB0aGlzLlVzaW5nTm90ID0gdHJ1ZTtcclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgLy8gQXJpdGhtZXRpYyBtZXRob2RzXHJcbiAgYWRkKGFtb3VudDogbnVtYmVyKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZEFyaXRobWV0aWNPcGVyYXRvcihhbW91bnQsICdhZGQnLCB0aGlzKTtcclxuICB9XHJcbiAgc3ViKGFtb3VudDogbnVtYmVyKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZEFyaXRobWV0aWNPcGVyYXRvcihhbW91bnQsICdzdWInLCB0aGlzKTtcclxuICB9XHJcbiAgbXVsKGFtb3VudDogbnVtYmVyKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZEFyaXRobWV0aWNPcGVyYXRvcihhbW91bnQsICdtdWwnLCB0aGlzKTtcclxuICB9XHJcbiAgZGl2KGFtb3VudDogbnVtYmVyKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZEFyaXRobWV0aWNPcGVyYXRvcihhbW91bnQsICdkaXYnLCB0aGlzKTtcclxuICB9XHJcbiAgbW9kKGFtb3VudDogbnVtYmVyKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZEFyaXRobWV0aWNPcGVyYXRvcihhbW91bnQsICdtb2QnLCB0aGlzKTtcclxuICB9XHJcblxyXG4gIC8vIFN0cmluZyBmdW5jdGlvbnNcclxuICBzdWJzdHJpbmdvZih2YWx1ZTogc3RyaW5nKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHRoaXMuUHJvcGVydHlJbmNsdWRlZCA9IHRydWU7XHJcbiAgICB0aGlzLkZ1bmNSZXR1cm5UeXBlID0gQm9vbGVhbigpO1xyXG4gICAgbGV0IHRoYXQgPSB0aGlzO1xyXG5cclxuICAgIGxldCBwcm9wZXJ0eSA9IHRoaXMuUHJvcGVydHk7XHJcbiAgICBpZiAodGhpcy50cmFuc2Zvcm1GdW5jICE9PSBudWxsKSB7XHJcbiAgICAgIHByb3BlcnR5ID0gdGhpcy5Db21wb25lbnRzW3RoaXMuQ29tcG9uZW50cy5sZW5ndGggLSAxXTtcclxuICAgICAgdGhpcy5Db21wb25lbnRzLnNwbGljZSh0aGlzLkNvbXBvbmVudHMubGVuZ3RoIC0gMSwgMSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5Db21wb25lbnRzLnB1c2goJ3N1YnN0cmluZ29mKFxcJycgKyB2YWx1ZSArICdcXCcsJyArIHByb3BlcnR5ICsgJyknKTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcbiAgc3Vic3RyaW5nKHBvc2l0aW9uOiBudW1iZXIsIGxlbmd0aD86IG51bWJlcik6IEZpbHRlckNsYXVzZSB7XHJcbiAgICB0aGlzLlByb3BlcnR5SW5jbHVkZWQgPSB0cnVlO1xyXG4gICAgdGhpcy5GdW5jUmV0dXJuVHlwZSA9IFN0cmluZygpO1xyXG5cclxuICAgIGxldCBjb21wcyA9IFt0aGlzLlByb3BlcnR5LCBwb3NpdGlvbl07XHJcbiAgICBpZiAobGVuZ3RoICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgY29tcHMucHVzaChsZW5ndGgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuQ29tcG9uZW50cy5wdXNoKCdzdWJzdHJpbmcoJyArIGNvbXBzLmpvaW4oJywnKSArICcpJyk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG4gIHRvTG93ZXIoKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHRoaXMuUHJvcGVydHlJbmNsdWRlZCA9IHRydWU7XHJcbiAgICB0aGlzLkZ1bmNSZXR1cm5UeXBlID0gU3RyaW5nKCk7XHJcbiAgICBsZXQgdGhhdCA9IHRoaXM7XHJcblxyXG4gICAgdGhpcy50cmFuc2Zvcm1GdW5jID0gdGhpcy50b0xvd2VyO1xyXG4gICAgdGhpcy5Db21wb25lbnRzLnB1c2goJ3RvbG93ZXIoJyArIHRoYXQuUHJvcGVydHkgKyAnKScpO1xyXG5cclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuICB0b1VwcGVyKCk6IEZpbHRlckNsYXVzZSB7XHJcbiAgICB0aGlzLlByb3BlcnR5SW5jbHVkZWQgPSB0cnVlO1xyXG4gICAgdGhpcy5GdW5jUmV0dXJuVHlwZSA9IFN0cmluZygpO1xyXG4gICAgbGV0IHRoYXQgPSB0aGlzO1xyXG5cclxuICAgIHRoaXMudHJhbnNmb3JtRnVuYyA9IHRoaXMudG9VcHBlcjtcclxuICAgIHRoaXMuQ29tcG9uZW50cy5wdXNoKCd0b3VwcGVyKCcgKyB0aGF0LlByb3BlcnR5ICsgJyknKTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcbiAgdHJpbSgpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgdGhpcy5Qcm9wZXJ0eUluY2x1ZGVkID0gdHJ1ZTtcclxuICAgIHRoaXMuRnVuY1JldHVyblR5cGUgPSBTdHJpbmcoKTtcclxuICAgIGxldCB0aGF0ID0gdGhpcztcclxuXHJcbiAgICB0aGlzLnRyYW5zZm9ybUZ1bmMgPSB0aGlzLnRyaW07XHJcbiAgICB0aGlzLkNvbXBvbmVudHMucHVzaCgndHJpbSgnICsgdGhhdC5Qcm9wZXJ0eSArICcpJyk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG4gIGVuZHN3aXRoKHZhbHVlOiBzdHJpbmcpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgdGhpcy5Qcm9wZXJ0eUluY2x1ZGVkID0gdHJ1ZTtcclxuICAgIHRoaXMuRnVuY1JldHVyblR5cGUgPSBCb29sZWFuKCk7XHJcbiAgICBsZXQgdGhhdCA9IHRoaXM7XHJcbiAgICB0aGlzLkNvbXBvbmVudHMucHVzaCgnZW5kc3dpdGgoJyArIHRoYXQuUHJvcGVydHkgKyAnLFxcJycgKyB2YWx1ZSArICdcXCcpJyk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG4gIHN0YXJ0c3dpdGgodmFsdWU6IHN0cmluZyk6IEZpbHRlckNsYXVzZSB7XHJcbiAgICB0aGlzLlByb3BlcnR5SW5jbHVkZWQgPSB0cnVlO1xyXG4gICAgdGhpcy5GdW5jUmV0dXJuVHlwZSA9IEJvb2xlYW4oKTtcclxuICAgIGxldCB0aGF0ID0gdGhpcztcclxuICAgIHRoaXMuQ29tcG9uZW50cy5wdXNoKCdzdGFydHN3aXRoKCcgKyB0aGF0LlByb3BlcnR5ICsgJyxcXCcnICsgdmFsdWUgKyAnXFwnKScpO1xyXG5cclxuICAgIHJldHVybiB0aGlzO1xyXG4gIH1cclxuICBsZW5ndGgoKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHRoaXMuUHJvcGVydHlJbmNsdWRlZCA9IHRydWU7XHJcbiAgICB0aGlzLkZ1bmNSZXR1cm5UeXBlID0gTnVtYmVyKCk7XHJcbiAgICBsZXQgdGhhdCA9IHRoaXM7XHJcbiAgICB0aGlzLkNvbXBvbmVudHMucHVzaCgnbGVuZ3RoKCcgKyB0aGF0LlByb3BlcnR5ICsgJyknKTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcbiAgaW5kZXhvZih2YWx1ZTogc3RyaW5nKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHRoaXMuUHJvcGVydHlJbmNsdWRlZCA9IHRydWU7XHJcbiAgICB0aGlzLkZ1bmNSZXR1cm5UeXBlID0gTnVtYmVyKCk7XHJcbiAgICBsZXQgdGhhdCA9IHRoaXM7XHJcbiAgICB0aGlzLkNvbXBvbmVudHMucHVzaCgnaW5kZXhvZignICsgdGhhdC5Qcm9wZXJ0eSArICcsXFwnJyArIHZhbHVlICsgJ1xcJyknKTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcbiAgcmVwbGFjZShmaW5kOiBzdHJpbmcsIHJlcGxhY2U6IHN0cmluZyk6IEZpbHRlckNsYXVzZSB7XHJcbiAgICB0aGlzLlByb3BlcnR5SW5jbHVkZWQgPSB0cnVlO1xyXG4gICAgdGhpcy5GdW5jUmV0dXJuVHlwZSA9IFN0cmluZygpO1xyXG4gICAgbGV0IHRoYXQgPSB0aGlzO1xyXG4gICAgdGhpcy5Db21wb25lbnRzLnB1c2goJ3JlcGxhY2UoJyArIHRoYXQuUHJvcGVydHkgKyAnLFxcJycgKyBmaW5kICsgJ1xcJyxcXCcnICsgcmVwbGFjZSArICdcXCcpJyk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICAvLyBDb25jYXRcclxuICBDb25jYXQoY29uY2F0OiBDb25jYXQpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgdGhpcy5Qcm9wZXJ0eUluY2x1ZGVkID0gdHJ1ZTtcclxuICAgIHRoaXMuRnVuY1JldHVyblR5cGUgPSBTdHJpbmcoKTtcclxuICAgIGxldCB0aGF0ID0gdGhpcztcclxuICAgIHRoYXQuQ29tcG9uZW50cy5wdXNoKGNvbmNhdC50b1N0cmluZygpKTtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIC8vIERhdGUgZnVuY3Rpb25zXHJcbiAgZGF5KCk6IEZpbHRlckNsYXVzZSB7XHJcbiAgICByZXR1cm4gSGVscGVycy5hZGRNZXRob2RXcmFwcGVyKHRoaXMsICdkYXknKTtcclxuICB9XHJcbiAgaG91cigpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgcmV0dXJuIEhlbHBlcnMuYWRkTWV0aG9kV3JhcHBlcih0aGlzLCAnaG91cicpO1xyXG4gIH1cclxuICBtaW51dGUoKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZE1ldGhvZFdyYXBwZXIodGhpcywgJ21pbnV0ZScpO1xyXG4gIH1cclxuICBtb250aCgpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgcmV0dXJuIEhlbHBlcnMuYWRkTWV0aG9kV3JhcHBlcih0aGlzLCAnbW9udGgnKTtcclxuICB9XHJcbiAgc2Vjb25kKCk6IEZpbHRlckNsYXVzZSB7XHJcbiAgICByZXR1cm4gSGVscGVycy5hZGRNZXRob2RXcmFwcGVyKHRoaXMsICdzZWNvbmQnKTtcclxuICB9XHJcbiAgeWVhcigpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgcmV0dXJuIEhlbHBlcnMuYWRkTWV0aG9kV3JhcHBlcih0aGlzLCAneWVhcicpO1xyXG4gIH1cclxuXHJcbiAgLy8gTWF0aCBmdW5jdGlvbnNcclxuICByb3VuZCgpOiBGaWx0ZXJDbGF1c2Uge1xyXG4gICAgcmV0dXJuIEhlbHBlcnMuYWRkTWV0aG9kV3JhcHBlcih0aGlzLCAncm91bmQnKTtcclxuICB9XHJcbiAgZmxvb3IoKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZE1ldGhvZFdyYXBwZXIodGhpcywgJ2Zsb29yJyk7XHJcbiAgfVxyXG4gIGNlaWxpbmcoKTogRmlsdGVyQ2xhdXNlIHtcclxuICAgIHJldHVybiBIZWxwZXJzLmFkZE1ldGhvZFdyYXBwZXIodGhpcywgJ2NlaWxpbmcnKTtcclxuICB9XHJcbn1cclxuIl19