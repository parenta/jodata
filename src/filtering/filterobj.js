"use strict";
var FilterObj = (function () {
    function FilterObj(filterObj, logicalOperator) {
        if (logicalOperator === void 0) { logicalOperator = null; }
        this.filterObj = filterObj;
        this.logicalOperator = null;
        if (logicalOperator !== undefined && logicalOperator !== null) {
            this.logicalOperator = logicalOperator;
        }
    }
    FilterObj.prototype.toString = function (i) {
        var filter = '';
        if (this.logicalOperator !== null && i > 0) {
            filter += ' ' + this.logicalOperator + ' ';
        }
        else if (i > 0 && this.logicalOperator === null) {
            filter += ' and ';
        }
        filter += this.filterObj.toString();
        return filter;
    };
    return FilterObj;
}());
exports.FilterObj = FilterObj;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyb2JqLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZmlsdGVyb2JqLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFFQTtJQUlFLG1CQUFhLFNBQXVDLEVBQUUsZUFBMkI7UUFBM0IsK0JBQTJCLEdBQTNCLHNCQUEyQjtRQUMvRSxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixFQUFFLENBQUMsQ0FBQyxlQUFlLEtBQUssU0FBUyxJQUFJLGVBQWUsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1FBQ3pDLENBQUM7SUFDSCxDQUFDO0lBR0QsNEJBQVEsR0FBUixVQUFTLENBQU07UUFDYixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsTUFBTSxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQztRQUM3QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xELE1BQU0sSUFBSSxPQUFPLENBQUM7UUFDcEIsQ0FBQztRQUVELE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3BDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUNILGdCQUFDO0FBQUQsQ0FBQyxBQXhCRCxJQXdCQztBQXhCWSxpQkFBUyxZQXdCckIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RmlsdGVyQ2xhdXNlfSBmcm9tICcuL2ZpbHRlcmNsYXVzZSc7XHJcbmltcG9ydCB7UHJlY2VkZW5jZUdyb3VwfSBmcm9tICcuL1ByZWNlZGVuY2VHcm91cCc7XHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJPYmoge1xyXG4gIGZpbHRlck9iajogRmlsdGVyQ2xhdXNlfFByZWNlZGVuY2VHcm91cDtcclxuICBsb2dpY2FsT3BlcmF0b3I6IGFueTtcclxuXHJcbiAgY29uc3RydWN0b3IgKGZpbHRlck9iajogRmlsdGVyQ2xhdXNlfFByZWNlZGVuY2VHcm91cCwgbG9naWNhbE9wZXJhdG9yOiBhbnkgPSBudWxsKSB7XHJcbiAgICB0aGlzLmZpbHRlck9iaiA9IGZpbHRlck9iajtcclxuICAgIHRoaXMubG9naWNhbE9wZXJhdG9yID0gbnVsbDtcclxuICAgIGlmIChsb2dpY2FsT3BlcmF0b3IgIT09IHVuZGVmaW5lZCAmJiBsb2dpY2FsT3BlcmF0b3IgIT09IG51bGwpIHtcclxuICAgICAgdGhpcy5sb2dpY2FsT3BlcmF0b3IgPSBsb2dpY2FsT3BlcmF0b3I7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcbiAgdG9TdHJpbmcoaTogYW55KSB7XHJcbiAgICBsZXQgZmlsdGVyID0gJyc7XHJcbiAgICBpZiAodGhpcy5sb2dpY2FsT3BlcmF0b3IgIT09IG51bGwgJiYgaSA+IDApIHtcclxuICAgICAgZmlsdGVyICs9ICcgJyArIHRoaXMubG9naWNhbE9wZXJhdG9yICsgJyAnO1xyXG4gICAgfSBlbHNlIGlmIChpID4gMCAmJiB0aGlzLmxvZ2ljYWxPcGVyYXRvciA9PT0gbnVsbCkge1xyXG4gICAgICBmaWx0ZXIgKz0gJyBhbmQgJztcclxuICAgIH1cclxuXHJcbiAgICBmaWx0ZXIgKz0gdGhpcy5maWx0ZXJPYmoudG9TdHJpbmcoKTtcclxuICAgIHJldHVybiBmaWx0ZXI7XHJcbiAgfVxyXG59XHJcbiJdfQ==