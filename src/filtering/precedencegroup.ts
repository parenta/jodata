import {FilterClause} from './filterclause';
import {FilterObj} from './filterobj';

export class PrecedenceGroup {

  private clauses: FilterObj[];

  constructor(filterClause: FilterClause = null) {
    this.clauses = [];
    if (filterClause !== undefined && filterClause !== null) {
      this.clauses.push(new FilterObj(filterClause, null));
    }
  }


  isEmpty() {
    return this.clauses.length === 0;
  };

  andFilter(filterClause: FilterClause): PrecedenceGroup {
    this.clauses.push(new FilterObj(filterClause, 'and'));
    return this;
  }

  orFilter(filterClause: FilterClause): PrecedenceGroup {
    this.clauses.push(new FilterObj(filterClause, 'or'));
    return this;
  }

  toString() {
    let filter: any, i: any;
    filter = '(';
    for (i = 0; i < this.clauses.length; i++) {
      filter += this.clauses[i].toString(i);
    }
    filter += ')';
    return filter;
  }
}
