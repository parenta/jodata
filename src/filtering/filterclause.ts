import {Helpers} from '../helpers/Helpers';
import {Concat} from '../Helpers/Concat';
export class FilterClause {

  Property: string;
  Components: string[];
  IsClauseEmpty: Boolean;
  PropertyIncluded: Boolean;
  private UsingNot: Boolean;
  Value: any;
  FuncReturnType: any;
  private transformFunc: Function;

  constructor(property: string = null) {
    this.Property = property;
    this.Components = [];
  };

  toString(): string {
    let strComps: any, i: any, filterStr: any;
    strComps = [];

    if (!this.PropertyIncluded) {
      strComps.push(this.Property);
    }

    for (i = 0; i < this.Components.length; i++) {
      strComps.push(this.Components[i]);
    }
    filterStr = strComps.join(' ');

    if (!this.UsingNot) {
      return filterStr;
    }

    return typeof this.FuncReturnType === 'boolean' ? 'not ' + filterStr : 'not (' + filterStr + ')';
  }
  isEmpty(): Boolean {
    return this.IsClauseEmpty || (this.PropertyIncluded && this.UsingNot);
  }



  // Logical operators
  eq(value: string|number|boolean): FilterClause {
    return Helpers.addLogicalOperator(value, 'eq', this);
  }
  ne(value: string|number|boolean): FilterClause {
    return Helpers.addLogicalOperator(value, 'ne', this);
  }
  gt(value: string|number|boolean): FilterClause {
    return Helpers.addLogicalOperator(value, 'gt', this);
  }
  ge(value: string|number|boolean): FilterClause {
    return Helpers.addLogicalOperator(value, 'ge', this);
  }
  lt(value: string|number|boolean): FilterClause {
    return Helpers.addLogicalOperator(value, 'lt', this);
  }
  le(value: string|number|boolean): FilterClause {
    return Helpers.addLogicalOperator(value, 'le', this);
  }
  not(): FilterClause {
    this.UsingNot = true;
    return this;
  }

  // Arithmetic methods
  add(amount: number): FilterClause {
    return Helpers.addArithmeticOperator(amount, 'add', this);
  }
  sub(amount: number): FilterClause {
    return Helpers.addArithmeticOperator(amount, 'sub', this);
  }
  mul(amount: number): FilterClause {
    return Helpers.addArithmeticOperator(amount, 'mul', this);
  }
  div(amount: number): FilterClause {
    return Helpers.addArithmeticOperator(amount, 'div', this);
  }
  mod(amount: number): FilterClause {
    return Helpers.addArithmeticOperator(amount, 'mod', this);
  }

  // String functions
  substringof(value: string): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = Boolean();
    let that = this;

    let property = this.Property;
    if (this.transformFunc !== null) {
      property = this.Components[this.Components.length - 1];
      this.Components.splice(this.Components.length - 1, 1);
    }

    this.Components.push('substringof(\'' + value + '\',' + property + ')');

    return this;
  }
  substring(position: number, length?: number): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = String();

    let comps = [this.Property, position];
    if (length !== undefined) {
      comps.push(length);
    }

    this.Components.push('substring(' + comps.join(',') + ')');

    return this;
  }
  toLower(): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = String();
    let that = this;

    this.transformFunc = this.toLower;
    this.Components.push('tolower(' + that.Property + ')');

    return this;
  }
  toUpper(): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = String();
    let that = this;

    this.transformFunc = this.toUpper;
    this.Components.push('toupper(' + that.Property + ')');

    return this;
  }
  trim(): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = String();
    let that = this;

    this.transformFunc = this.trim;
    this.Components.push('trim(' + that.Property + ')');

    return this;
  }
  endswith(value: string): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = Boolean();
    let that = this;
    this.Components.push('endswith(' + that.Property + ',\'' + value + '\')');

    return this;
  }
  startswith(value: string): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = Boolean();
    let that = this;
    this.Components.push('startswith(' + that.Property + ',\'' + value + '\')');

    return this;
  }
  length(): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = Number();
    let that = this;
    this.Components.push('length(' + that.Property + ')');

    return this;
  }
  indexof(value: string): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = Number();
    let that = this;
    this.Components.push('indexof(' + that.Property + ',\'' + value + '\')');

    return this;
  }
  replace(find: string, replace: string): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = String();
    let that = this;
    this.Components.push('replace(' + that.Property + ',\'' + find + '\',\'' + replace + '\')');

    return this;
  }

  // Concat
  Concat(concat: Concat): FilterClause {
    this.PropertyIncluded = true;
    this.FuncReturnType = String();
    let that = this;
    that.Components.push(concat.toString());

    return this;
  }

  // Date functions
  day(): FilterClause {
    return Helpers.addMethodWrapper(this, 'day');
  }
  hour(): FilterClause {
    return Helpers.addMethodWrapper(this, 'hour');
  }
  minute(): FilterClause {
    return Helpers.addMethodWrapper(this, 'minute');
  }
  month(): FilterClause {
    return Helpers.addMethodWrapper(this, 'month');
  }
  second(): FilterClause {
    return Helpers.addMethodWrapper(this, 'second');
  }
  year(): FilterClause {
    return Helpers.addMethodWrapper(this, 'year');
  }

  // Math functions
  round(): FilterClause {
    return Helpers.addMethodWrapper(this, 'round');
  }
  floor(): FilterClause {
    return Helpers.addMethodWrapper(this, 'floor');
  }
  ceiling(): FilterClause {
    return Helpers.addMethodWrapper(this, 'ceiling');
  }
}
