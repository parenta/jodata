import {FilterClause} from './filterclause';
import {PrecedenceGroup} from './PrecedenceGroup';
export class FilterObj {
  filterObj: FilterClause|PrecedenceGroup;
  logicalOperator: any;

  constructor (filterObj: FilterClause|PrecedenceGroup, logicalOperator: any = null) {
    this.filterObj = filterObj;
    this.logicalOperator = null;
    if (logicalOperator !== undefined && logicalOperator !== null) {
      this.logicalOperator = logicalOperator;
    }
  }


  toString(i: any) {
    let filter = '';
    if (this.logicalOperator !== null && i > 0) {
      filter += ' ' + this.logicalOperator + ' ';
    } else if (i > 0 && this.logicalOperator === null) {
      filter += ' and ';
    }

    filter += this.filterObj.toString();
    return filter;
  }
}
