import { FilterClause } from './filterclause';
import { PrecedenceGroup } from './PrecedenceGroup';
export declare class FilterObj {
    filterObj: FilterClause | PrecedenceGroup;
    logicalOperator: any;
    constructor(filterObj: FilterClause | PrecedenceGroup, logicalOperator?: any);
    toString(i: any): string;
}
