"use strict";
var filterobj_1 = require('./filterobj');
var PrecedenceGroup = (function () {
    function PrecedenceGroup(filterClause) {
        if (filterClause === void 0) { filterClause = null; }
        this.clauses = [];
        if (filterClause !== undefined && filterClause !== null) {
            this.clauses.push(new filterobj_1.FilterObj(filterClause, null));
        }
    }
    PrecedenceGroup.prototype.isEmpty = function () {
        return this.clauses.length === 0;
    };
    ;
    PrecedenceGroup.prototype.andFilter = function (filterClause) {
        this.clauses.push(new filterobj_1.FilterObj(filterClause, 'and'));
        return this;
    };
    PrecedenceGroup.prototype.orFilter = function (filterClause) {
        this.clauses.push(new filterobj_1.FilterObj(filterClause, 'or'));
        return this;
    };
    PrecedenceGroup.prototype.toString = function () {
        var filter, i;
        filter = '(';
        for (i = 0; i < this.clauses.length; i++) {
            filter += this.clauses[i].toString(i);
        }
        filter += ')';
        return filter;
    };
    return PrecedenceGroup;
}());
exports.PrecedenceGroup = PrecedenceGroup;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUHJlY2VkZW5jZUdyb3VwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUHJlY2VkZW5jZUdyb3VwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSwwQkFBd0IsYUFBYSxDQUFDLENBQUE7QUFFdEM7SUFJRSx5QkFBWSxZQUFpQztRQUFqQyw0QkFBaUMsR0FBakMsbUJBQWlDO1FBQzNDLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLEVBQUUsQ0FBQyxDQUFDLFlBQVksS0FBSyxTQUFTLElBQUksWUFBWSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxxQkFBUyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELENBQUM7SUFDSCxDQUFDO0lBR0QsaUNBQU8sR0FBUDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7SUFFRCxtQ0FBUyxHQUFULFVBQVUsWUFBMEI7UUFDbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxxQkFBUyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3RELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsa0NBQVEsR0FBUixVQUFTLFlBQTBCO1FBQ2pDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUkscUJBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNyRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFDRSxJQUFJLE1BQVcsRUFBRSxDQUFNLENBQUM7UUFDeEIsTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUNiLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekMsTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFDRCxNQUFNLElBQUksR0FBRyxDQUFDO1FBQ2QsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBQ0gsc0JBQUM7QUFBRCxDQUFDLEFBbkNELElBbUNDO0FBbkNZLHVCQUFlLGtCQW1DM0IsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RmlsdGVyQ2xhdXNlfSBmcm9tICcuL2ZpbHRlcmNsYXVzZSc7XHJcbmltcG9ydCB7RmlsdGVyT2JqfSBmcm9tICcuL2ZpbHRlcm9iaic7XHJcblxyXG5leHBvcnQgY2xhc3MgUHJlY2VkZW5jZUdyb3VwIHtcclxuXHJcbiAgcHJpdmF0ZSBjbGF1c2VzOiBGaWx0ZXJPYmpbXTtcclxuXHJcbiAgY29uc3RydWN0b3IoZmlsdGVyQ2xhdXNlOiBGaWx0ZXJDbGF1c2UgPSBudWxsKSB7XHJcbiAgICB0aGlzLmNsYXVzZXMgPSBbXTtcclxuICAgIGlmIChmaWx0ZXJDbGF1c2UgIT09IHVuZGVmaW5lZCAmJiBmaWx0ZXJDbGF1c2UgIT09IG51bGwpIHtcclxuICAgICAgdGhpcy5jbGF1c2VzLnB1c2gobmV3IEZpbHRlck9iaihmaWx0ZXJDbGF1c2UsIG51bGwpKTtcclxuICAgIH1cclxuICB9XHJcblxyXG5cclxuICBpc0VtcHR5KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuY2xhdXNlcy5sZW5ndGggPT09IDA7XHJcbiAgfTtcclxuXHJcbiAgYW5kRmlsdGVyKGZpbHRlckNsYXVzZTogRmlsdGVyQ2xhdXNlKTogUHJlY2VkZW5jZUdyb3VwIHtcclxuICAgIHRoaXMuY2xhdXNlcy5wdXNoKG5ldyBGaWx0ZXJPYmooZmlsdGVyQ2xhdXNlLCAnYW5kJykpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICBvckZpbHRlcihmaWx0ZXJDbGF1c2U6IEZpbHRlckNsYXVzZSk6IFByZWNlZGVuY2VHcm91cCB7XHJcbiAgICB0aGlzLmNsYXVzZXMucHVzaChuZXcgRmlsdGVyT2JqKGZpbHRlckNsYXVzZSwgJ29yJykpO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICB0b1N0cmluZygpIHtcclxuICAgIGxldCBmaWx0ZXI6IGFueSwgaTogYW55O1xyXG4gICAgZmlsdGVyID0gJygnO1xyXG4gICAgZm9yIChpID0gMDsgaSA8IHRoaXMuY2xhdXNlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICBmaWx0ZXIgKz0gdGhpcy5jbGF1c2VzW2ldLnRvU3RyaW5nKGkpO1xyXG4gICAgfVxyXG4gICAgZmlsdGVyICs9ICcpJztcclxuICAgIHJldHVybiBmaWx0ZXI7XHJcbiAgfVxyXG59XHJcbiJdfQ==