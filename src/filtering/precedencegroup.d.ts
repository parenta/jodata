import { FilterClause } from './filterclause';
export declare class PrecedenceGroup {
    private clauses;
    constructor(filterClause?: FilterClause);
    isEmpty(): boolean;
    andFilter(filterClause: FilterClause): PrecedenceGroup;
    orFilter(filterClause: FilterClause): PrecedenceGroup;
    toString(): any;
}
