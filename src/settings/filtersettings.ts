import {FilterObj} from '../filtering/filterobj';
export class FilterSettings  {

  Filters: FilterObj[];
  DefaultFilters: FilterObj[];
  CapturedFilter: FilterObj[];
    private filter: string;

  constructor() {
    this.Filters = [];
    this.DefaultFilters = [];
    this.CapturedFilter = [];
      this.filter = null;
  }



  toString(): string {
    let allFilters: any, i: any, filter: any;

    allFilters = [];
    filter = '$filter=';
    if (this.DefaultFilters.length > 0) {
      for (i = 0; i < this.DefaultFilters.length; i++) {
        allFilters.push(this.DefaultFilters[i]);
      }
    }

    for (i = 0; i < this.Filters.length; i++) {
      allFilters.push(this.Filters[i]);
    }

    for (i = 0; i < allFilters.length; i++) {
      filter += allFilters[i].toString(i);
    }

    return encodeURI(filter);
  }

  reset(): void {
    this.Filters = [];
    if (this.CapturedFilter.length > 0) {
      for (let i = 0; i < this.CapturedFilter.length; i++) {
        this.Filters.push(this.CapturedFilter[i]);
      }
    }
  }

  fullReset(): void {
    this.Filters = [];
    this.CapturedFilter = [];
  }

  isSet(): boolean {
    return this.Filters.length > 0 || this.DefaultFilters.length > 0;
  }
}
