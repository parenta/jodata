export class SkipSettings {
  Skip: number;
  DefaultSkip: number;

  constructor() {
    this.Skip = null;
    this.DefaultSkip = null;
  }

  toString() {
    return encodeURI('$skip=' + (this.Skip !== null ? this.Skip : this.DefaultSkip));
  }
  reset() {
    this.Skip = null;
  }
  isSet() {
    return this.Skip !== null || this.DefaultSkip !== null;
  }
}
