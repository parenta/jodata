export declare class SkipSettings {
    Skip: number;
    DefaultSkip: number;
    constructor();
    toString(): string;
    reset(): void;
    isSet(): boolean;
}
