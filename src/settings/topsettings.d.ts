export declare class TopSettings {
    Top: number;
    DefaultTop: number;
    constructor();
    toString(): string;
    reset(): void;
    isSet(): boolean;
}
