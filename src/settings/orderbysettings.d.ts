export declare class OrderBySettings {
    Property: string;
    Order: string;
    DefaultProperty: string;
    DefaultOrder: string;
    constructor();
    toString(): string;
    reset(): void;
    isSet(): boolean;
}
