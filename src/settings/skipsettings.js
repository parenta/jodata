"use strict";
var SkipSettings = (function () {
    function SkipSettings() {
        this.Skip = null;
        this.DefaultSkip = null;
    }
    SkipSettings.prototype.toString = function () {
        return encodeURI('$skip=' + (this.Skip !== null ? this.Skip : this.DefaultSkip));
    };
    SkipSettings.prototype.reset = function () {
        this.Skip = null;
    };
    SkipSettings.prototype.isSet = function () {
        return this.Skip !== null || this.DefaultSkip !== null;
    };
    return SkipSettings;
}());
exports.SkipSettings = SkipSettings;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2tpcHNldHRpbmdzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2tpcHNldHRpbmdzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtJQUlFO1FBQ0UsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQztJQUVELCtCQUFRLEdBQVI7UUFDRSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDbkYsQ0FBQztJQUNELDRCQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNuQixDQUFDO0lBQ0QsNEJBQUssR0FBTDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQztJQUN6RCxDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLEFBbEJELElBa0JDO0FBbEJZLG9CQUFZLGVBa0J4QixDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFNraXBTZXR0aW5ncyB7XHJcbiAgU2tpcDogbnVtYmVyO1xyXG4gIERlZmF1bHRTa2lwOiBudW1iZXI7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5Ta2lwID0gbnVsbDtcclxuICAgIHRoaXMuRGVmYXVsdFNraXAgPSBudWxsO1xyXG4gIH1cclxuXHJcbiAgdG9TdHJpbmcoKSB7XHJcbiAgICByZXR1cm4gZW5jb2RlVVJJKCckc2tpcD0nICsgKHRoaXMuU2tpcCAhPT0gbnVsbCA/IHRoaXMuU2tpcCA6IHRoaXMuRGVmYXVsdFNraXApKTtcclxuICB9XHJcbiAgcmVzZXQoKSB7XHJcbiAgICB0aGlzLlNraXAgPSBudWxsO1xyXG4gIH1cclxuICBpc1NldCgpIHtcclxuICAgIHJldHVybiB0aGlzLlNraXAgIT09IG51bGwgfHwgdGhpcy5EZWZhdWx0U2tpcCAhPT0gbnVsbDtcclxuICB9XHJcbn1cclxuIl19