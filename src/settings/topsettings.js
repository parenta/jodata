"use strict";
var TopSettings = (function () {
    function TopSettings() {
        this.Top = null;
        this.DefaultTop = null;
    }
    TopSettings.prototype.toString = function () {
        return encodeURI('$top=' + (this.Top !== null ? this.Top : this.DefaultTop));
    };
    TopSettings.prototype.reset = function () {
        this.Top = null;
    };
    TopSettings.prototype.isSet = function () {
        return this.Top !== null || this.DefaultTop !== null;
    };
    return TopSettings;
}());
exports.TopSettings = TopSettings;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9wc2V0dGluZ3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0b3BzZXR0aW5ncy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFJRTtRQUNFLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQ3pCLENBQUM7SUFFRCw4QkFBUSxHQUFSO1FBQ0UsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFDRCwyQkFBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7SUFDbEIsQ0FBQztJQUNELDJCQUFLLEdBQUw7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUM7SUFDdkQsQ0FBQztJQUNILGtCQUFDO0FBQUQsQ0FBQyxBQWxCRCxJQWtCQztBQWxCWSxtQkFBVyxjQWtCdkIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBUb3BTZXR0aW5ncyB7XHJcbiAgVG9wOiBudW1iZXI7XHJcbiAgRGVmYXVsdFRvcDogbnVtYmVyO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuVG9wID0gbnVsbDtcclxuICAgIHRoaXMuRGVmYXVsdFRvcCA9IG51bGw7XHJcbiAgfVxyXG5cclxuICB0b1N0cmluZygpIHtcclxuICAgIHJldHVybiBlbmNvZGVVUkkoJyR0b3A9JyArICh0aGlzLlRvcCAhPT0gbnVsbCA/IHRoaXMuVG9wIDogdGhpcy5EZWZhdWx0VG9wKSk7XHJcbiAgfVxyXG4gIHJlc2V0KCkge1xyXG4gICAgdGhpcy5Ub3AgPSBudWxsO1xyXG4gIH1cclxuICBpc1NldCgpIHtcclxuICAgIHJldHVybiB0aGlzLlRvcCAhPT0gbnVsbCB8fCB0aGlzLkRlZmF1bHRUb3AgIT09IG51bGw7XHJcbiAgfVxyXG59XHJcbiJdfQ==