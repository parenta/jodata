export class TopSettings {
  Top: number;
  DefaultTop: number;

  constructor() {
    this.Top = null;
    this.DefaultTop = null;
  }

  toString() {
    return encodeURI('$top=' + (this.Top !== null ? this.Top : this.DefaultTop));
  }
  reset() {
    this.Top = null;
  }
  isSet() {
    return this.Top !== null || this.DefaultTop !== null;
  }
}
