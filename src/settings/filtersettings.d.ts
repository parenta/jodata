import { FilterObj } from '../filtering/filterobj';
export declare class FilterSettings {
    Filters: FilterObj[];
    DefaultFilters: FilterObj[];
    CapturedFilter: FilterObj[];
    private filter;
    constructor();
    toString(): string;
    reset(): void;
    fullReset(): void;
    isSet(): boolean;
}
