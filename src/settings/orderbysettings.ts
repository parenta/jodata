export class OrderBySettings {
    Property: string;
    Order: string;
    DefaultProperty: string;
    DefaultOrder: string;

    constructor() {
        this.Property = null;
        this.Order = null;
        this.DefaultProperty = null;
        this.DefaultOrder = null;
    }

    toString(): string {
        let qsValue = '$orderby=' + (this.Property || this.DefaultProperty);
        if (this.DefaultOrder !== null || this.Order !== null) {
            qsValue += ' ' + (this.Order || this.DefaultOrder);
        }

        return encodeURI(qsValue);
    }

    reset(): void {
        this.Property = null;
        this.Order = null;
    }

    isSet(): boolean {
        return this.Property !== null || this.DefaultProperty !== null;
    }
}
