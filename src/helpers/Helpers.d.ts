import { FilterClause } from '../filtering/FilterClause';
export declare class Helpers {
    static formatValue(value: string): string;
    static addLogicalOperator(value: any, operator: string, filterClause: FilterClause): FilterClause;
    static addArithmeticOperator(amount: any, operator: string, filterClause: FilterClause): FilterClause;
    static addMethodWrapper(filterClause: FilterClause, func: string): FilterClause;
}
