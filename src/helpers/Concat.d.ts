export declare class Concat {
    LeftSide: string | Concat;
    RightSide: string | Concat;
    constructor(value1: string | Concat, value2: string | Concat);
    toString(): string;
}
