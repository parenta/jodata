export class Concat {
  LeftSide: string|Concat;
  RightSide: string|Concat;

  constructor(value1: string|Concat, value2: string|Concat) {
    this.LeftSide = value1;
    this.RightSide = value2;
  }

  toString(): string {
    let that = this;

    function writeValue(value: any) {
      if (typeof value === 'object') {
        return value.toString();
      }
      if (typeof value === 'function') {
        return value.call(that);
      }
      return value.toString();
    }

    return 'concat(' + writeValue(this.LeftSide) + ',' + writeValue(this.RightSide) + ')';
  }
}
