import {FilterClause} from '../filtering/FilterClause';
export class Helpers {
  static formatValue(value: string): string {
    if (value.length > 8 && value.substring(0, 8) === 'datetime') {
      return value;
    }

    if (value.length > 4 && value.substring(0, 4) === 'guid') {
      return value;
    }

    if (typeof value === 'string') {
      let numberSuffixes = ['m', 'f', 'd'];
      for (let i = 0; i < numberSuffixes.length; i++) {
        let suffix = numberSuffixes[i];
        if (value.indexOf(suffix, value.length - suffix.length) !== -1) {
          let numberValue = value.substring(0, value.length - 1);
          if (!isNaN(Number(numberValue))) {
            return value;
          }
        }
      }
      return '\'' + value + '\'';
    }
    return value;
  }

  static addLogicalOperator (value: any, operator: string, filterClause: FilterClause): FilterClause {
    filterClause.Value = value;
    filterClause.IsClauseEmpty = false;

    filterClause.Components.push(operator + ' ' + this.formatValue(value));

    return filterClause;
  }

  static addArithmeticOperator (amount: any, operator: string, filterClause: FilterClause): FilterClause {
    filterClause.Components.push(operator + ' ' + amount);
    return filterClause;
  }

  static addMethodWrapper (filterClause: FilterClause , func: string): FilterClause  {
    filterClause.PropertyIncluded = true;
    filterClause.FuncReturnType = Number();
    let that = filterClause;
    filterClause.Components.push(func + '(' + that.Property + ')');

    return filterClause;
  }
}
