import {FilterSettings} from './settings/filtersettings';
import {OrderBySettings} from './settings/orderbysettings';
import {SkipSettings} from './settings/skipsettings';
import {TopSettings} from './settings/topsettings';
import {FilterObj} from './filtering/FilterObj';
import {FilterClause} from './filtering/FilterClause';
import {PrecedenceGroup} from './filtering/PrecedenceGroup';

export class Jo {
    private filterSettings: FilterSettings;
    private orderBySettings: OrderBySettings;
    private skipSettings: SkipSettings;
    private topSettings: TopSettings;
    private currentHashRoute: string = '';

    // Casts
    private static literal(stringLiteral: string): string {
        return '\'' + stringLiteral.toString() + '\'';
    };

    private static datetime(stringLiteral: string): string {
        return 'datetime\'' + stringLiteral + '\'';
    };

    private static guid(stringLiteral: string): string {
        return 'guid\'' + stringLiteral + '\'';
    }

    private static decimal(stringLiteral: number): string {
        return stringLiteral + 'm';
    };

    private static double(stringLiteral: number): string {
        return stringLiteral + 'd';
    }

    private static single(stringLiteral: number): string {
        return stringLiteral + 'f';
    }


    private updateHashRoute(hashRoute: string): void {
        this.currentHashRoute = hashRoute;
    };


    constructor(private baseUri: string) {
        this.filterSettings = new FilterSettings();
        this.orderBySettings = new OrderBySettings();
        this.skipSettings = new SkipSettings();
        this.topSettings = new TopSettings();
    }

    // Order by
    setOrderByDefault(property: string, order?: string): Jo {
        this.orderBySettings.DefaultProperty = property;
        this.orderBySettings.DefaultOrder = order === undefined ? 'desc' : order;
        return this;
    }

    orderBy(property: string): Jo {
        this.orderBySettings.Property = property;
        return this;
    }

    desc(): Jo {
        this.orderBySettings.Order = 'desc';
        return this;
    }

    asc(): Jo {
        this.orderBySettings.Order = 'asc';
        return this;
    }

    resetOrderBy(): Jo {
        this.orderBySettings.reset();
        return this;
    }

    // Top
    setTopDefault(top: number): Jo {
        this.topSettings.DefaultTop = top;
        return this;
    }

    top(top: number): Jo {
        this.topSettings.Top = top;
        return this;
    }

    resetTop(): Jo {
        this.topSettings.reset();
        return this;
    }

    // Skip
    setSkipDefault(skip: number): Jo {
        this.skipSettings.DefaultSkip = skip;
        return this;
    }

    skip(skip: number): Jo {
        this.skipSettings.Skip = skip;
        return this;
    }

    resetSkip(): Jo {
        this.skipSettings.reset();
        return this;
    }

    // Filter
    filter(filterClause: FilterClause|PrecedenceGroup): Jo {
        this.filterSettings.Filters.push(new FilterObj(filterClause));
        return this;
    }

    andFilter(filterClause: FilterClause|PrecedenceGroup): Jo {
        this.filterSettings.Filters.push(new FilterObj(filterClause, 'and'));
        return this;
    }

    orFilter(filterClause: FilterClause|PrecedenceGroup): Jo {
        this.filterSettings.Filters.push(new FilterObj(filterClause, 'or'));
        return this;
    }

    removeFilter(property: string): Jo {
        let i: any;

        if (!this.filterSettings.isSet()) {
            return this;
        }

        for (i = 0; i < this.filterSettings.Filters.length; i++) {
            if ((<any>this.filterSettings.Filters[i].filterObj).Property === property) {
                this.filterSettings.Filters.splice(i, 1);
            }
        }

        return this;
    }

    private captureFilter(): void {
        this.filterSettings.CapturedFilter = [];
        for (let i = 0; i < this.filterSettings.Filters.length; i++) {
            this.filterSettings.CapturedFilter.push(this.filterSettings.Filters[i]);
        }
    }

    resetFilter(): Jo {
        this.filterSettings.fullReset();
        return this;
    }

    resetToCapturedFilter(): Jo {
        this.filterSettings.reset();
        return this;
    }


    toString(): string {
        let url: any, components: any;

        url = this.baseUri;
        components = [];

        if (this.orderBySettings.isSet()) {
            components.push(this.orderBySettings.toString());
        }

        if (this.topSettings.isSet()) {
            components.push(this.topSettings.toString());
        }

        if (this.skipSettings.isSet()) {
            components.push(this.skipSettings.toString());
        }

        if (this.filterSettings.isSet()) {
            components.push(this.filterSettings.toString());
        }
        return components.length > 0 ? url + '?' + components.join('&') : url;
    }
}
